package com.naga.share.market.fundamental.analysis.services.annual;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.services.annual.constants.AnnualConstants;
import com.naga.share.market.fundamental.analysis.services.annual.utils.AnnualUtils;
import com.naga.share.market.fundamental.analysis.utils.CalculationUtils;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class AnnualCalculationServiceImpl implements AnnualCalculationService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Override
	public void annualCalculation(Map<String, Object> inputParams) {
		logger.info("AnnualCalculationServiceImpl - annualCalculation method - start");
		basicDetailsCalculation(inputParams);
		profitabilityRatiosCalculation(inputParams);
		leverageRatiosCalculation(inputParams);
		operatingRatiosCalculation(inputParams);
		valuationRatiosCalculation(inputParams);
		crucialDetailsCheckCalculation(inputParams);
		logger.info("AnnualCalculationServiceImpl - annualCalculation method - end");
	}

	@Override
	public void basicDetailsCalculation(Map<String, Object> inputParams) {
		logger.info("AnnualCalculationServiceImpl - basicDetailsCalculation method - start");
		Annual annualEntity = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		annualEntity.setOperatingRevenue(annualEntity.getTotalRevenue() - annualEntity.getOtherIncome());
		annualEntity.setNetSales(
				annualEntity.getNetSales() == 0 ? annualEntity.getOperatingRevenue() : annualEntity.getNetSales());
		annualEntity.setCostOfGoodsSold(annualEntity.getCostOfMaterialsConsumed()
				+ annualEntity.getConsumptionOfStoresAndSpareParts() + annualEntity.getPowerAndFuelConsumption());
		annualEntity.setGrossProfit(annualEntity.getNetSales() - annualEntity.getCostOfGoodsSold());
		annualEntity.setOperatingExpenses(annualEntity.getTotalExpenses() - annualEntity.getFinanceCost()
				- annualEntity.getDepreciationAndAmortization());
		annualEntity.setEarningsBfrIncomeTaxDepriAndAmor(
				annualEntity.getOperatingRevenue() - annualEntity.getOperatingExpenses());
		annualEntity.setEarningsBeforeIncomeTax(
				annualEntity.getEarningsBfrIncomeTaxDepriAndAmor() - annualEntity.getDepreciationAndAmortization());
		annualEntity.setProfitBeforeTax((annualEntity.getTotalRevenue() - annualEntity.getTotalExpenses())
				+ annualEntity.getExpectionalItemsBeforeTax());
		annualEntity.setProfitAfterTax(annualEntity.getProfitBeforeTax() - annualEntity.getApplicableTax());
		annualEntity.setShareCapital(annualEntity.getFaceValue() * annualEntity.getTotalEquityShares());
		annualEntity
				.setAverageShareHoldersFund(CalculationUtils.average(annualEntity.getShareHoldersFundAndTotalEquity(),
						annualEntity.getPriorYearShareHoldersFundAndTotalEquity()));
		annualEntity.setOverallCapitalEmployed(annualEntity.getShortTermBorrowings()
				+ annualEntity.getLongTermBorrowings() + annualEntity.getShareHoldersFundAndTotalEquity());
		annualEntity.setTotalDebt(annualEntity.getShortTermBorrowings() + annualEntity.getLongTermBorrowings());
		annualEntity.setTotalAssets(annualEntity.getNonCurrentAssets() + annualEntity.getCurrentAssets());
		annualEntity.setTotalLiabilities(annualEntity.getCurrentLiabilities() + annualEntity.getNonCurrentLiabilities()
				+ annualEntity.getShareHoldersFundAndTotalEquity());
		annualEntity.setAverageTradeReceivables(CalculationUtils.average(annualEntity.getTradeReceivables(),
				annualEntity.getPriorYearTradeReceivables()));
		annualEntity.setAverageInventory(
				CalculationUtils.average(annualEntity.getInventory(), annualEntity.getPriorYearInventory()));
		annualEntity.setPriorYearTotalAssets(
				annualEntity.getPriorYearCurrentAssets() + annualEntity.getPriorYearNonCurrentAssets());
		annualEntity.setPriorYearTotalLiabilities(
				annualEntity.getPriorYearCurrentLiabilities() + annualEntity.getPriorYearNonCurrentLiabilities()
						+ annualEntity.getPriorYearShareHoldersFundAndTotalEquity());
		annualEntity.setAverageTotalAssets(
				CalculationUtils.average(annualEntity.getTotalAssets(), annualEntity.getPriorYearTotalAssets()));
		annualEntity.setAverageFixedAssets(
				CalculationUtils.average(annualEntity.getFixedAssets(), annualEntity.getPriorYearFixedAssets()));
		annualEntity.setWorkingCapital(annualEntity.getCurrentAssets() - annualEntity.getCurrentLiabilities());
		annualEntity.setPriorYearWorkingCapital(
				annualEntity.getPriorYearCurrentAssets() - annualEntity.getPriorYearCurrentLiabilities());
		annualEntity.setAverageWorkingCapital(
				CalculationUtils.average(annualEntity.getWorkingCapital(), annualEntity.getPriorYearWorkingCapital()));
		annualEntity.setFreeCashFlow(
				annualEntity.getCashFlowFromOperatingActivities() + annualEntity.getCapitalExpenditure());
		annualEntity.setCashFlow(
				annualEntity.getCashFlowFromOperatingActivities() + annualEntity.getCashFlowFromFinancialActivities()
						+ annualEntity.getCashFlowFromInvestingActivities());
		annualEntity.setCashAndCashEquivalentAtYearEnding(
				annualEntity.getCashFlow() + annualEntity.getCashAndCashEquivalentAtYearOpening()
						+ annualEntity.getEffectOfForeignExchangeCashAndCashEquivalent());
		logger.info("AnnualCalculationServiceImpl - basicDetailsCalculation method - end");
	}

	@Override
	public void profitabilityRatiosCalculation(Map<String, Object> inputParams) {
		Annual annualEntity = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		annualEntity.setGrossProfitMargin(
				CalculationUtils.margin(annualEntity.getGrossProfit(), annualEntity.getNetSales()));
		annualEntity.setEarningsBfrIncomeTaxDepriAndAmorMargin(CalculationUtils
				.margin(annualEntity.getEarningsBfrIncomeTaxDepriAndAmor(), annualEntity.getOperatingRevenue()));
		annualEntity.setProfitAfterTaxMargin(
				CalculationUtils.margin(annualEntity.getProfitAfterTax(), annualEntity.getTotalRevenue()));
		annualEntity.setNetProfitMargin(
				CalculationUtils.margin(annualEntity.getProfitAfterTax(), annualEntity.getOperatingRevenue()));
		annualEntity.setReturnOnCapitalEmployed(
				CalculationUtils.margin(annualEntity.getProfitBeforeTax(), annualEntity.getOverallCapitalEmployed()));
		annualEntity.setReturnOnAssets(CalculationUtils.margin(
				(annualEntity.getProfitAfterTax()
						+ (annualEntity.getFinanceCost() * AnnualConstants.FINANCE_COST_COMPUTE_VALUE)),
				annualEntity.getAverageTotalAssets()));
		logger.info("AnnualCalculationServiceImpl - profitabilityRatiosCalculation method - end");
	}

	@Override
	public void leverageRatiosCalculation(Map<String, Object> inputParams) {
		logger.info("AnnualCalculationServiceImpl - leverageRatiosCalculation method - start");
		Annual annualEntity = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		annualEntity
				.setInterestCoverageRatio(annualEntity.getEarningsBeforeIncomeTax() / annualEntity.getFinanceCost());
		annualEntity
				.setDebtToEquityRatio(annualEntity.getTotalDebt() / annualEntity.getShareHoldersFundAndTotalEquity());
		annualEntity.setFinancialLeverageRatio(
				annualEntity.getAverageTotalAssets() / annualEntity.getAverageShareHoldersFund());
		annualEntity.setDebtToAssestRatio(annualEntity.getTotalDebt() / annualEntity.getTotalAssets());
		logger.info("AnnualCalculationServiceImpl - leverageRatiosCalculation method - end");
	}

	@Override
	public void operatingRatiosCalculation(Map<String, Object> inputParams) {
		logger.info("AnnualCalculationServiceImpl - operatingRatiosCalculation method - start");
		Annual annualEntity = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		annualEntity.setInventoryTurnoverRatio(annualEntity.getCostOfGoodsSold() / annualEntity.getAverageInventory());
		annualEntity
				.setInventoryNoOfDays(AnnualConstants.NO_OF_DAYS_IN_A_YEAR / annualEntity.getInventoryTurnoverRatio());
		annualEntity.setAccountsReceivableTurnoverRatio(
				annualEntity.getOperatingRevenue() / annualEntity.getAverageTradeReceivables());
		annualEntity.setDaysSalesOutstanding(
				AnnualConstants.NO_OF_DAYS_IN_A_YEAR / annualEntity.getAccountsReceivableTurnoverRatio());
		annualEntity
				.setFixedAssetsTurnoverRatio(annualEntity.getOperatingRevenue() / annualEntity.getAverageFixedAssets());
		annualEntity
				.setTotalAssetsTurnoverRatio(annualEntity.getOperatingRevenue() / annualEntity.getAverageTotalAssets());
		annualEntity.setWorkingCapitalTurnoverRatio(
				annualEntity.getOperatingRevenue() / annualEntity.getAverageWorkingCapital());
		logger.info("AnnualCalculationServiceImpl - operatingRatiosCalculation method - end");

	}

	@Override
	public void valuationRatiosCalculation(Map<String, Object> inputParams) {
		logger.info("AnnualCalculationServiceImpl - valuationRatiosCalculation method - start");
		Annual annualEntity = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		annualEntity.setSalesPerShare(
				AnnualUtils.perShareCalculation(annualEntity.getTotalRevenue(), annualEntity.getTotalEquityShares()));
		annualEntity.setPriceToSalesRatio(
				(annualEntity.getTotalRevenue() * AnnualConstants.BASE_VALUE) / annualEntity.getSalesPerShare());
		annualEntity.setEarningsPerShare(
				AnnualUtils.perShareCalculation(annualEntity.getProfitAfterTax(), annualEntity.getTotalEquityShares()));
		annualEntity.setBookValuePerShare(AnnualUtils.perShareCalculation(
				(annualEntity.getShareHoldersFundAndTotalEquity() - annualEntity.getRevaluationReserves()),
				annualEntity.getTotalEquityShares()));
		annualEntity.setPriceToBookValueRatio(annualEntity.getSharePrice() / annualEntity.getBookValuePerShare());
		annualEntity.setPriceToEarningsRatio(annualEntity.getSharePrice() / annualEntity.getEarningsPerShare());
		annualEntity.setReturnOnEquity(annualEntity.getNetProfitMargin() * annualEntity.getTotalAssetsTurnoverRatio()
				* annualEntity.getFinancialLeverageRatio());
		logger.info("AnnualCalculationServiceImpl - valuationRatiosCalculation method - end");
	}

	@Override
	public void crucialDetailsCheckCalculation(Map<String, Object> inputParams) {
		logger.info("AnnualCalculationServiceImpl - crucialDetailsCheckCalculation method - start");
		Annual annualEntity = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		annualEntity.setGrossProfitMarginCheck(AnnualUtils.grossProfitMarginCheck(annualEntity.getGrossProfitMargin()));
		annualEntity.setEBITDAMarginCheck(
				AnnualUtils.earningsBfrIncTaxDepNAmorMarginCheck(annualEntity.getEarningsBfrIncomeTaxDepriAndAmor()));
		annualEntity.setProfitAfterTaxMarginCheck(
				AnnualUtils.profitAfterTaxMarginCheck(annualEntity.getProfitAfterTaxMargin()));
		annualEntity.setReturnOnEquityCheck(AnnualUtils.retuenOnEquityCheck(annualEntity.getReturnOnEquity()));
		annualEntity.setReturnOnAssetsCheck(AnnualUtils.returnOnAssetsCheck(annualEntity.getReturnOnAssets()));
		annualEntity.setReturnOnCapitalEmployedCheck(
				AnnualUtils.returnOnCapitalEmployedCheck(annualEntity.getReturnOnCapitalEmployed()));
		annualEntity.setInterestCoverageRatioCheck(
				AnnualUtils.interestCoverageRatioCheck(annualEntity.getInterestCoverageRatio()));
		annualEntity.setDebtToEquityRatioCheck(AnnualUtils.debtToEquityRatioCheck(annualEntity.getDebtToEquityRatio()));
		annualEntity.setDebtToAssetsRatioCheck(AnnualUtils.debtToAssetsRatioCheck(annualEntity.getDebtToAssestRatio()));
		annualEntity.setFinancialLeverageRatioCheck(
				AnnualUtils.financialLeverageRatioCheck(annualEntity.getFinancialLeverageRatio()));
		annualEntity.setFixedAssetsTurnoverRatioCheck(
				AnnualUtils.fixedAssetsTurnoverRatioCheck(annualEntity.getFixedAssetsTurnoverRatio()));
		annualEntity.setTotalAssetsTurnoverRatioCheck(
				AnnualUtils.totalAssetsTurnoverRatioCheck(annualEntity.getTotalAssetsTurnoverRatio()));
		annualEntity.setWorkingCapitalTurnoverRatioCheck(
				AnnualUtils.workingCapitalTurnoverRatioCheck(annualEntity.getWorkingCapitalTurnoverRatio()));
		annualEntity.setPriceToSalesRatioCheck(AnnualUtils.priceToSalesRatioCheck(annualEntity.getPriceToSalesRatio()));
		annualEntity
				.setPriceToBookRatioCheck(AnnualUtils.priceToBookRatioCheck(annualEntity.getPriceToBookValueRatio()));
		annualEntity.setPriceToEarningsRatioCheck(
				AnnualUtils.priceToEarningsRatioCheck(annualEntity.getPriceToEarningsRatio()));
		logger.info("AnnualCalculationServiceImpl - crucialDetailsCheckCalculation method - end");
	}

}
