package com.naga.share.market.fundamental.analysis.services.annual;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.annual.response.AnnualResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface AnnualDataRetrivalService {

	/**
	 * 
	 * A Service method to retrieve all the annually analyzed details of a company
	 * 
	 * @param inputParams
	 * @throws NoDataException
	 */
	public AnnualResponseDTO retriveAnalyzedDetailsForACompanyAndAnalysisYear(Map<String, Object> inputParams)
			throws NoDataException;

	/**
	 * A service method to assign basic details of a company from database
	 * 
	 * @param annualResponse
	 * @param allAnnuallyAnalyzedDetails
	 */
	public void assignRetrivedBasicDetailsResponse(AnnualResponseDTO annualResponse, Annual allAnnuallyAnalyzedDetails);

	/**
	 * 
	 * A service method to assign input details retrieved from database
	 * 
	 * @param annualEntity
	 * @return AnnualRequestDTO
	 */
	public void assignRetrivedAnnualAnalysisInputResponse(AnnualResponseDTO annualResponse, Annual annualEntity);

	/**
	 * A service method to assign analyzed details retrieved from database
	 * 
	 * @param annualEntity
	 * @return AnnuallyAnalyzedDetails
	 * 
	 */
	public void assignRetrivedAnnuallyAnalyzedDetailsResponse(AnnualResponseDTO annualResponse, Annual annualEntity);

}
