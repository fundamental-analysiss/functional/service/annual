package com.naga.share.market.fundamental.analysis.services.annual.utils;

import com.naga.share.market.fundamental.analysis.services.annual.constants.AnnualConstants;

/**
 * @author Nagaaswin S
 *
 */
public final class AnnualUtils {

	private AnnualUtils() {
	}

	public static double perShareCalculation(double value, double totalEquityShares) {
		return (value * AnnualConstants.BASE_VALUE) / totalEquityShares;
	}

	public static String grossProfitMarginCheck(double grossProfitMargin) {
		if (grossProfitMargin >= 20.0) {
			return AnnualConstants.GOOD;
		} else if (grossProfitMargin >= 15.0 && grossProfitMargin < 20.0) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String earningsBfrIncTaxDepNAmorMarginCheck(double earningsBfrIncTaxDepNAmorMargin) {
		if (earningsBfrIncTaxDepNAmorMargin >= 15.0) {
			return AnnualConstants.GOOD;
		} else if (earningsBfrIncTaxDepNAmorMargin < 15.00 && earningsBfrIncTaxDepNAmorMargin >= 10.00) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String profitAfterTaxMarginCheck(double profitAfterTaxMargin) {
		if (profitAfterTaxMargin > 10.00) {
			return AnnualConstants.GOOD;
		} else if (profitAfterTaxMargin >= 5.00 && profitAfterTaxMargin <= 10.00) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String retuenOnEquityCheck(double returnOnEquity) {
		if (returnOnEquity > 20.00) {
			return AnnualConstants.GOOD;
		} else if (returnOnEquity <= 20.0 && returnOnEquity >= 15.00) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String returnOnAssetsCheck(double returnOnAssets) {
		if (returnOnAssets > 17.0) {
			return AnnualConstants.GOOD;
		} else if (returnOnAssets <= 17.0 && returnOnAssets > 12.0) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String returnOnCapitalEmployedCheck(double returnOnCapitalEmployed) {
		if (returnOnCapitalEmployed > 30.00) {
			return AnnualConstants.GOOD;
		} else if (returnOnCapitalEmployed >= 20.00 && returnOnCapitalEmployed <= 30.00) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String interestCoverageRatioCheck(double interestCoverageRatio) {
		if (interestCoverageRatio > 3.00) {
			return AnnualConstants.GOOD;
		} else if (interestCoverageRatio > 2.00 && interestCoverageRatio <= 3.00) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String debtToEquityRatioCheck(double debtToEquityRatio) {
		if (debtToEquityRatio < 1.0) {
			return AnnualConstants.GOOD;
		} else if (debtToEquityRatio < 1.25) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String debtToAssetsRatioCheck(double debtToAssetsRatio) {
		if (debtToAssetsRatio < 0.3) {
			return AnnualConstants.GOOD;
		} else if (debtToAssetsRatio >= 0.3 && debtToAssetsRatio < 0.4) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String financialLeverageRatioCheck(double financialLeverageRatio) {
		if (financialLeverageRatio < 1.5) {
			return AnnualConstants.GOOD;
		} else if (financialLeverageRatio >= 1.5 && financialLeverageRatio < 2.0) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String fixedAssetsTurnoverRatioCheck(double fixedAssetsTurnoverRatio) {
		if (fixedAssetsTurnoverRatio > 3) {
			return AnnualConstants.GOOD;
		} else if (fixedAssetsTurnoverRatio <= 3 && fixedAssetsTurnoverRatio > 2) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String totalAssetsTurnoverRatioCheck(double totalAssetsTurnoverRatio) {
		if (totalAssetsTurnoverRatio > 1.5) {
			return AnnualConstants.GOOD;
		} else if (totalAssetsTurnoverRatio <= 1.5 && totalAssetsTurnoverRatio > 1.0) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String workingCapitalTurnoverRatioCheck(double workingCapitalTurnoverRatio) {
		if (workingCapitalTurnoverRatio > 3) {
			return AnnualConstants.GOOD;
		} else if (workingCapitalTurnoverRatio <= 3 && workingCapitalTurnoverRatio > 2) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String priceToSalesRatioCheck(double priceToSalesRatio) {
		if (priceToSalesRatio < 1) {
			return AnnualConstants.GOOD;
		} else if (priceToSalesRatio >= 1 && priceToSalesRatio <= 2) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String priceToBookRatioCheck(double priceToBookRatio) {
		if (priceToBookRatio < 1) {
			return AnnualConstants.GOOD;
		} else if (priceToBookRatio < 5 && priceToBookRatio >= 1) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}

	public static String priceToEarningsRatioCheck(double priceToEarningsRatio) {
		if (priceToEarningsRatio < 10) {
			return AnnualConstants.GOOD;
		} else if (priceToEarningsRatio >= 10 && priceToEarningsRatio <= 20) {
			return AnnualConstants.AVERAGE;
		} else {
			return AnnualConstants.BAD;
		}
	}
}
