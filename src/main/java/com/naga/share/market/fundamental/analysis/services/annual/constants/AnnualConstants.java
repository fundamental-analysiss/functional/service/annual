package com.naga.share.market.fundamental.analysis.services.annual.constants;

/**
 * @author Nagaaswin S
 *
 */
public class AnnualConstants {

	private AnnualConstants() {
	}

	public static final String ANNUAL_RESPONSE = "annualResponse";
	public static final String ANNUAL_REQUEST = "annualRequest";
	public static final String ANNUAL_ENTITY = "annualEntity";
	public static final String ALL_ANNUALLY_ANALYZED_DETAILS = "allAnnuallyAnalyzedDetails";
	public static final String SECTOR = "sector";
	public static final String INDUSTRY = "industry";
	public static final String COMPANY = "company";
	public static final int BASE_VALUE = 100000;
	public static final double FINANCE_COST_COMPUTE_VALUE = 0.68;
	public static final String GOOD = "good";
	public static final String AVERAGE = "average";
	public static final String BAD = "bad";
	public static final int NO_OF_DAYS_IN_A_YEAR = 365;
	public static final String ANALYSIS_YEAR = "analysisYear";
	public static final String IS_PRESENT = "isPresent";
	public static final String ANALYZER_ID = "analyzerId";

//	#ROLES
	public static final String MAINTAINER = "MAINTAINER";
	public static final String ANALYZER = "ANALYZER";
}
