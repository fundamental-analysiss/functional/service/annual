package com.naga.share.market.fundamental.analysis.services.annual;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.annual.AnnualDAO;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.annual.request.AnnualAnalyzeRequestDTO;
import com.naga.share.market.fundamental.analysis.model.annual.response.AnnualResponseDTO;
import com.naga.share.market.fundamental.analysis.model.annual.response.AnnuallyAnalyzedDetails;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;
import com.naga.share.market.fundamental.analysis.model.industry.response.IndustryResponseDTO;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;
import com.naga.share.market.fundamental.analysis.model.sector.response.SectorResponseDTO;
import com.naga.share.market.fundamental.analysis.services.annual.constants.AnnualConstants;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class AnnualDataRetrivalServiceImpl implements AnnualDataRetrivalService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	AnnualDAO annualDAO;

	@Override
	public AnnualResponseDTO retriveAnalyzedDetailsForACompanyAndAnalysisYear(Map<String, Object> inputParams)
			throws NoDataException {
		logger.info("AnnualDataRetrivalServiceImpl - retriveAnalyzedDetailsForACompanyAndAnalysisYear method - start");
		AnnualResponseDTO annualResponse = new AnnualResponseDTO();
		Company company = (Company) inputParams.get(AnnualConstants.COMPANY);
		Annual annualEntity = annualDAO.findByCompanyAndAnalysisYear(company,
				(int) inputParams.get(AnnualConstants.ANALYSIS_YEAR));
		if (ValidationUtils.isNotNull(annualEntity)) {
			assignRetrivedBasicDetailsResponse(annualResponse, annualEntity);
			assignRetrivedAnnualAnalysisInputResponse(annualResponse, annualEntity);
			assignRetrivedAnnuallyAnalyzedDetailsResponse(annualResponse, annualEntity);
			logger.info("AnnualDataRetrivalServiceImpl - allAnalyzedDetailsForACompany method - end");
			return annualResponse;
		} else {
			logger.error(
					"AnnualDataRetrivalServiceImpl - retriveAnalyzedDetailsForACompanyAndAnalysisYear Method - Annually analyzed details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}

	}

	@Override
	public void assignRetrivedBasicDetailsResponse(AnnualResponseDTO annualResponse, Annual annuallyAnalyzedDetails) {
		logger.info("AnnualDataRetrivalServiceImpl - assignRetrivedBasicDetailsResponse method - end");
		Sector sector = annuallyAnalyzedDetails.getSector();
		SectorResponseDTO sectorResponse = new SectorResponseDTO();
		sectorResponse.setSectorName(sector.getSectorName());
		Industry industry = annuallyAnalyzedDetails.getIndustry();
		IndustryResponseDTO industryResponse = new IndustryResponseDTO();
		industryResponse.setIndustryName(industry.getIndustryName());
		Company company = annuallyAnalyzedDetails.getCompany();
		CompanyResponseDTO companyResponse = new CompanyResponseDTO();
		companyResponse.setCompanyName(company.getCompanyName());
		companyResponse.setCompanySymbol(company.getCompanySymbol());
		annualResponse.setSector(sectorResponse);
		annualResponse.setIndustry(industryResponse);
		annualResponse.setCompany(companyResponse);
		logger.info("AnnualDataRetrivalServiceImpl - assignRetrivedBasicDetailsResponse method - end");
	}

	@Override
	public void assignRetrivedAnnualAnalysisInputResponse(AnnualResponseDTO annualResponse, Annual annualEntity) {
		logger.info("AnnualDataRetrivalServiceImpl - assignRetrivedAnnualAnalysisInputResponse method - start");
		AnnualAnalyzeRequestDTO annualAnalysisInput = new AnnualAnalyzeRequestDTO();
		annualAnalysisInput.setAnalysisYear(annualEntity.getAnalysisYear());
		annualAnalysisInput.setTotalRevenue(annualEntity.getTotalRevenue());
		annualAnalysisInput.setNetSales(annualEntity.getNetSales());
		annualAnalysisInput.setOtherIncome(annualEntity.getOtherIncome());
		annualAnalysisInput.setTotalExpenses(annualEntity.getTotalExpenses());
		annualAnalysisInput.setCostOfMaterialsConsumed(annualEntity.getCostOfMaterialsConsumed());
		annualAnalysisInput.setPurchaseOfStockInTrade(annualEntity.getPurchaseOfStockInTrade());
		annualAnalysisInput.setPowerAndFuelConsumption(annualEntity.getPowerAndFuelConsumption());
		annualAnalysisInput.setConsumptionOfStoresAndSpareParts(annualEntity.getConsumptionOfStoresAndSpareParts());
		annualAnalysisInput.setFinanceCost(annualEntity.getFinanceCost());
		annualAnalysisInput.setDepreciationAndAmortization(annualEntity.getDepreciationAndAmortization());
		annualAnalysisInput.setExpectionalItemsBeforeTax(annualEntity.getExpectionalItemsBeforeTax());
		annualAnalysisInput.setApplicableTax(annualEntity.getApplicableTax());
		annualAnalysisInput.setSharePrice(annualEntity.getSharePrice());
		annualAnalysisInput.setTotalEquityShares(annualEntity.getTotalEquityShares());
		annualAnalysisInput.setFaceValue(annualEntity.getFaceValue());
		annualAnalysisInput.setShareHoldersFundAndTotalEquity(annualEntity.getShareHoldersFundAndTotalEquity());
		annualAnalysisInput.setRevaluationReserves(annualEntity.getRevaluationReserves());
		annualAnalysisInput.setPriorYearShareHoldersFund(annualEntity.getPriorYearShareHoldersFundAndTotalEquity());
		annualAnalysisInput.setNonCurrentLiabilities(annualEntity.getNonCurrentLiabilities());
		annualAnalysisInput.setLongTermBorrowings(annualEntity.getLongTermBorrowings());
		annualAnalysisInput.setCurrentLiabilities(annualEntity.getCurrentLiabilities());
		annualAnalysisInput.setShortTermBorrowings(annualEntity.getShortTermBorrowings());
		annualAnalysisInput.setPriorYearNonCurrentLiabilities(annualEntity.getPriorYearNonCurrentLiabilities());
		annualAnalysisInput.setPriorYearCurrentLiabilities(annualEntity.getPriorYearCurrentLiabilities());
		annualAnalysisInput.setNonCurrentAssets(annualEntity.getNonCurrentAssets());
		annualAnalysisInput.setFixedAssets(annualEntity.getFixedAssets());
		annualAnalysisInput.setCurrentAssets(annualEntity.getCurrentAssets());
		annualAnalysisInput.setInventory(annualEntity.getInventory());
		annualAnalysisInput.setTradeReceivables(annualEntity.getTradeReceivables());
		annualAnalysisInput.setPriorYearNonCurrentAssets(annualEntity.getPriorYearNonCurrentAssets());
		annualAnalysisInput.setPriorYearFixedAssets(annualEntity.getPriorYearFixedAssets());
		annualAnalysisInput.setPriorYearCurrentAssets(annualEntity.getPriorYearCurrentAssets());
		annualAnalysisInput.setPriorYearInventory(annualEntity.getPriorYearInventory());
		annualAnalysisInput.setPriorYearTradeReceivables(annualEntity.getPriorYearTradeReceivables());
		annualAnalysisInput.setCashFlowFromOperatingActivities(annualEntity.getCashFlowFromOperatingActivities());
		annualAnalysisInput.setCapitalExpenditure(annualEntity.getCapitalExpenditure());
		annualAnalysisInput.setCashFlowFromInvestingActivities(annualEntity.getCashFlowFromInvestingActivities());
		annualAnalysisInput.setCashFlowFromFinancialActivities(annualEntity.getCashFlowFromFinancialActivities());
		annualAnalysisInput.setCashAndCashEquivalentAtYearOpening(annualEntity.getCashAndCashEquivalentAtYearOpening());
		annualAnalysisInput.setEffectOfForeignExchangeCashAndCashEquivalent(
				annualEntity.getEffectOfForeignExchangeCashAndCashEquivalent());
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getReportURL())) {
			annualAnalysisInput.setReportUrl(annualEntity.getReportURL());
		}
		logger.debug(
				"AnnualDataRetrivalServiceImpl - assignRetrivedAnnualAnalysisInputResponse method - Annual Analysis Input: {}",
				annualAnalysisInput.getCompanyName());
		annualResponse.setAnnualAnalysisInput(annualAnalysisInput);
		logger.info("AnnualDataRetrivalServiceImpl - assignRetrivedAnnualAnalysisInputResponse method - end");
	}

	@Override
	public void assignRetrivedAnnuallyAnalyzedDetailsResponse(AnnualResponseDTO annualResponse, Annual annualEntity) {
		logger.info("AnnualDataRetrivalServiceImpl - assignRetrivedAnnuallyAnalyzedDetailsResponse method - start");
		AnnuallyAnalyzedDetails annuallyAnalyzedDetails = new AnnuallyAnalyzedDetails();
		annuallyAnalyzedDetails.setAnalysisYear(annualEntity.getAnalysisYear());
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getGrossProfitMarginCheck())) {
			annuallyAnalyzedDetails.setGrossProfitMarginCheck(annualEntity.getGrossProfitMarginCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getEBITDAMarginCheck())) {
			annuallyAnalyzedDetails.setEBITDAMarginCheck(annualEntity.getEBITDAMarginCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getProfitAfterTaxMarginCheck())) {
			annuallyAnalyzedDetails.setProfitAfterTaxMarginCheck(annualEntity.getProfitAfterTaxMarginCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getReturnOnEquityCheck())) {
			annuallyAnalyzedDetails.setReturnOnEquityCheck(annualEntity.getReturnOnEquityCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getReturnOnAssetsCheck())) {
			annuallyAnalyzedDetails.setReturnOnAssetsCheck(annualEntity.getReturnOnAssetsCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getReturnOnCapitalEmployedCheck())) {
			annuallyAnalyzedDetails.setReturnOnCapitalEmployedCheck(annualEntity.getReturnOnCapitalEmployedCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getInterestCoverageRatioCheck())) {
			annuallyAnalyzedDetails.setInterestCoverageRatioCheck(annualEntity.getInterestCoverageRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getDebtToEquityRatioCheck())) {
			annuallyAnalyzedDetails.setDebtToEquityRatioCheck(annualEntity.getDebtToEquityRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getDebtToAssetsRatioCheck())) {
			annuallyAnalyzedDetails.setDebtToAssetsRatioCheck(annualEntity.getDebtToAssetsRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getFinancialLeverageRatioCheck())) {
			annuallyAnalyzedDetails.setFinancialLeverageRatioCheck(annualEntity.getFinancialLeverageRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getFixedAssetsTurnoverRatioCheck())) {
			annuallyAnalyzedDetails.setFixedAssetsTurnoverRatioCheck(annualEntity.getFixedAssetsTurnoverRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getTotalAssetsTurnoverRatioCheck())) {
			annuallyAnalyzedDetails.setTotalAssetsTurnoverRatioCheck(annualEntity.getTotalAssetsTurnoverRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getWorkingCapitalTurnoverRatioCheck())) {
			annuallyAnalyzedDetails
					.setWorkingCapitalTurnoverRatioCheck(annualEntity.getWorkingCapitalTurnoverRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getPriceToSalesRatioCheck())) {
			annuallyAnalyzedDetails.setPriceToSalesRatioCheck(annualEntity.getPriceToSalesRatioCheck());
		}

		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getPriceToBookRatioCheck())) {
			annuallyAnalyzedDetails.setPriceToBookRatioCheck(annualEntity.getPriceToBookRatioCheck());
		}
		if (ValidationUtils.isNotNullAndEmpty(annualEntity.getPriceToEarningsRatioCheck())) {
			annuallyAnalyzedDetails.setPriceToEarningsRatioCheck(annualEntity.getPriceToEarningsRatioCheck());
		}
		annuallyAnalyzedDetails.setGrossProfit(annualEntity.getGrossProfit());
		annuallyAnalyzedDetails.setEarningsBfrIncomeTaxDepriAndAmor(annualEntity.getEarningsBfrIncomeTaxDepriAndAmor());
		annuallyAnalyzedDetails.setProfitBeforeTax(annualEntity.getProfitBeforeTax());
		annuallyAnalyzedDetails.setProfitAfterTax(annualEntity.getProfitAfterTax());
		annuallyAnalyzedDetails.setTotalAssets(annualEntity.getTotalAssets());
		annuallyAnalyzedDetails.setCashFlow(annualEntity.getCashFlow());
		annuallyAnalyzedDetails
				.setCashAndCashEquivalentAtYearEnding(annualEntity.getCashAndCashEquivalentAtYearEnding());
		annuallyAnalyzedDetails.setGrossProfitMargin(annualEntity.getGrossProfitMargin());
		annuallyAnalyzedDetails
				.setEarningsBfrIncomeTaxDepriAndAmorMargin(annualEntity.getEarningsBfrIncomeTaxDepriAndAmorMargin());
		annuallyAnalyzedDetails.setProfitAfterTaxMargin(annualEntity.getProfitAfterTaxMargin());
		annuallyAnalyzedDetails.setNetProfitMargin(annualEntity.getNetProfitMargin());
		annuallyAnalyzedDetails.setReturnOnEquity(annualEntity.getReturnOnEquity());
		annuallyAnalyzedDetails.setReturnOnAssets(annualEntity.getReturnOnAssets());
		annuallyAnalyzedDetails.setReturnOnCapitalEmployed(annualEntity.getReturnOnCapitalEmployed());
		annuallyAnalyzedDetails.setInterestCoverageRatio(annualEntity.getInterestCoverageRatio());
		annuallyAnalyzedDetails.setDebtToEquityRatio(annualEntity.getDebtToEquityRatio());
		annuallyAnalyzedDetails.setDebtToAssestRatio(annualEntity.getDebtToAssestRatio());
		annuallyAnalyzedDetails.setFinancialLeverageRatio(annualEntity.getFinancialLeverageRatio());
		annuallyAnalyzedDetails.setFixedAssetsTurnoverRatio(annualEntity.getFixedAssetsTurnoverRatio());
		annuallyAnalyzedDetails.setTotalAssetsTurnoverRatio(annualEntity.getTotalAssetsTurnoverRatio());
		annuallyAnalyzedDetails.setWorkingCapitalTurnoverRatio(annualEntity.getWorkingCapitalTurnoverRatio());
		annuallyAnalyzedDetails.setInventoryTurnoverRatio(annualEntity.getInventoryTurnoverRatio());
		annuallyAnalyzedDetails.setInventoryNoOfDays(annualEntity.getInventoryNoOfDays());
		annuallyAnalyzedDetails.setAccountsReceivableTurnoverRatio(annualEntity.getAccountsReceivableTurnoverRatio());
		annuallyAnalyzedDetails.setDaysSalesOutstanding(annualEntity.getDaysSalesOutstanding());
		annuallyAnalyzedDetails.setSalesPerShare(annualEntity.getSalesPerShare());
		annuallyAnalyzedDetails.setBookValuePerShare(annualEntity.getBookValuePerShare());
		annuallyAnalyzedDetails.setEarningsPerShare(annualEntity.getEarningsPerShare());
		annuallyAnalyzedDetails.setPriceToSalesRatio(annualEntity.getPriceToSalesRatio());
		annuallyAnalyzedDetails.setPriceToBookValueRatio(annualEntity.getPriceToBookValueRatio());
		annuallyAnalyzedDetails.setPriceToEarningsRatio(annualEntity.getPriceToEarningsRatio());
		logger.debug(
				"AnnualDataRetrivalServiceImpl - assignRetrivedAnnuallyAnalyzedDetailsResponse method - Annually Analyzed Details: {}",
				annuallyAnalyzedDetails.getAnalysisYear());
		annualResponse.setAnnualAnalyzedDetails(annuallyAnalyzedDetails);
		logger.info("AnnualDataRetrivalServiceImpl - assignRetrivedAnnuallyAnalyzedDetailsResponse method - end");

	}

}
