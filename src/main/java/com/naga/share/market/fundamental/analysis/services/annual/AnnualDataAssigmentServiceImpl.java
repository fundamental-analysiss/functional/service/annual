package com.naga.share.market.fundamental.analysis.services.annual;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.annual.AnnualDAO;
import com.naga.share.market.fundamental.analysis.adaptor.company.CompanyDAO;
import com.naga.share.market.fundamental.analysis.adaptor.industry.IndustryDAO;
import com.naga.share.market.fundamental.analysis.adaptor.sector.SectorDAO;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.annual.request.AnnualAnalyzeRequestDTO;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;
import com.naga.share.market.fundamental.analysis.services.annual.constants.AnnualConstants;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class AnnualDataAssigmentServiceImpl implements AnnualDataAssigmentService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	SectorDAO sectorDAO;

	@Autowired
	IndustryDAO industryDAO;

	@Autowired
	CompanyDAO companyDAO;

	@Autowired
	AnnualDAO annualDAO;

	@Override
	public void assignSector(Map<String, Object> inputParams) throws NoDataException {
		logger.info("AnnualDataAssigmentServiceImpl - assignSector method - start");
		AnnualAnalyzeRequestDTO annualRequest = (AnnualAnalyzeRequestDTO) inputParams
				.get(AnnualConstants.ANNUAL_REQUEST);
		String analyzerId = (String) inputParams.get(AnnualConstants.ANALYZER_ID);
		String sectorName = annualRequest.getSector();
		Sector sector = sectorDAO.findBySectorName(sectorName);
		if (!ValidationUtils.isNotNull(sector)) {
			sector = new Sector();
			sector.setCreatedBy(analyzerId);
			sector.setLastUpdatedBy(analyzerId);
			sector.setSectorName(sectorName);
			sectorDAO.insertSector(sector);
		} else {
			sector.setLastUpdatedBy(analyzerId);
			sectorDAO.updateSector(sector);
		}
		inputParams.put(AnnualConstants.SECTOR, sector);
		logger.info("AnnualDataAssigmentServiceImpl - assignSector method - end");

	}

	@Override
	public void assignIndustry(Map<String, Object> inputParams) throws NoDataException {
		logger.info("AnnualDataAssigmentServiceImpl - assignIndustry method - start");
		AnnualAnalyzeRequestDTO annualRequest = (AnnualAnalyzeRequestDTO) inputParams
				.get(AnnualConstants.ANNUAL_REQUEST);
		String analyzerId = (String) inputParams.get(AnnualConstants.ANALYZER_ID);
		Sector sector = (Sector) inputParams.get(AnnualConstants.SECTOR);
		String industryName = annualRequest.getIndustry();
		Industry industry = industryDAO.findByIndustryName(industryName);
		if (!ValidationUtils.isNotNull(industry)) {
			industry = new Industry();
			industry.setIndustryName(industryName);
			industry.setCreatedBy(analyzerId);
			industry.setLastUpdatedBy(analyzerId);
			industry.setSector(sector);
			sector.addIndustry(industry);
			industryDAO.insertIndustry(industry);
		} else {
			industry.setLastUpdatedBy(analyzerId);
			industry.setSector(sector);
			sector.addIndustry(industry);
			industryDAO.updateIndustry(industry);
		}
		inputParams.put(AnnualConstants.INDUSTRY, industry);
		logger.info("AnnualDataAssigmentServiceImpl - assignIndustry method - end");

	}

	@Override
	public void assignCompany(Map<String, Object> inputParams) throws NoDataException {
		logger.info("AnnualDataAssigmentServiceImpl - assignCompany method - start");
		AnnualAnalyzeRequestDTO annualRequest = (AnnualAnalyzeRequestDTO) inputParams
				.get(AnnualConstants.ANNUAL_REQUEST);
		Sector sector = (Sector) inputParams.get(AnnualConstants.SECTOR);
		Industry industry = (Industry) inputParams.get(AnnualConstants.INDUSTRY);
		String analyzerId = (String) inputParams.get(AnnualConstants.ANALYZER_ID);
		String companyName = annualRequest.getCompanyName();
		Company company = companyDAO.findByCompanyName(companyName);
		if (!ValidationUtils.isNotNull(company)) {
			company = new Company();
			company.setCompanyName(companyName);
			company.setCompanySymbol(annualRequest.getCompanySymbol());
			company.setCreatedBy(analyzerId);
			company.setLastUpdatedBy(analyzerId);
			company.setSector(sector);
			company.setIndustry(industry);
			sector.addCompany(company);
			industry.addCompany(company);
			companyDAO.insertCompany(company);
		} else {
			company.setLastUpdatedBy(analyzerId);
			company.setSector(sector);
			company.setIndustry(industry);
			sector.addCompany(company);
			industry.addCompany(company);
			companyDAO.updateCompany(company);
		}
		inputParams.put(AnnualConstants.COMPANY, company);
		logger.info("AnnualDataAssigmentServiceImpl - assignCompany method - end");

	}

	@Override
	public void assignAnnualInput(Map<String, Object> inputParams) {
		logger.info("AnnualDataAssigmentServiceImpl - assignAnnualInput method - start");
		AnnualAnalyzeRequestDTO annualRequest = (AnnualAnalyzeRequestDTO) inputParams
				.get(AnnualConstants.ANNUAL_REQUEST);
		Annual annualEntity = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		Sector sector = (Sector) inputParams.get(AnnualConstants.SECTOR);
		Industry industry = (Industry) inputParams.get(AnnualConstants.INDUSTRY);
		Company company = (Company) inputParams.get(AnnualConstants.COMPANY);
		boolean isPresent = (boolean) inputParams.get(AnnualConstants.IS_PRESENT);
		if (!isPresent) {
			sector.addAnnual(annualEntity);
			industry.addAnnual(annualEntity);
			company.addAnnual(annualEntity);
		}
		annualEntity.setSector(sector);
		annualEntity.setIndustry(industry);
		annualEntity.setCompany(company);
		annualEntity.setAnalysisYear(annualRequest.getAnalysisYear());
		annualEntity.setTotalRevenue(annualRequest.getTotalRevenue());
		annualEntity.setNetSales(annualRequest.getNetSales());
		annualEntity.setOtherIncome(annualRequest.getOtherIncome());
		annualEntity.setTotalExpenses(annualRequest.getTotalExpenses());
		annualEntity.setCostOfMaterialsConsumed(annualRequest.getCostOfMaterialsConsumed());
		annualEntity.setPurchaseOfStockInTrade(annualRequest.getPurchaseOfStockInTrade());
		annualEntity.setPowerAndFuelConsumption(annualRequest.getPowerAndFuelConsumption());
		annualEntity.setConsumptionOfStoresAndSpareParts(annualRequest.getConsumptionOfStoresAndSpareParts());
		annualEntity.setFinanceCost(annualRequest.getFinanceCost());
		annualEntity.setDepreciationAndAmortization(annualRequest.getDepreciationAndAmortization());
		annualEntity.setExpectionalItemsBeforeTax(annualRequest.getExpectionalItemsBeforeTax());
		annualEntity.setApplicableTax(annualRequest.getApplicableTax());
		annualEntity.setSharePrice(annualRequest.getSharePrice());
		annualEntity.setTotalEquityShares(annualRequest.getTotalEquityShares());
		annualEntity.setFaceValue(annualRequest.getFaceValue());
		annualEntity.setShareHoldersFundAndTotalEquity(annualRequest.getShareHoldersFundAndTotalEquity());
		annualEntity.setRevaluationReserves(annualRequest.getRevaluationReserves());
		annualEntity.setPriorYearShareHoldersFundAndTotalEquity(annualRequest.getPriorYearShareHoldersFund());
		annualEntity.setNonCurrentLiabilities(annualRequest.getNonCurrentLiabilities());
		annualEntity.setLongTermBorrowings(annualRequest.getLongTermBorrowings());
		annualEntity.setCurrentLiabilities(annualRequest.getCurrentLiabilities());
		annualEntity.setShortTermBorrowings(annualRequest.getShortTermBorrowings());
		annualEntity.setPriorYearNonCurrentLiabilities(annualRequest.getPriorYearNonCurrentLiabilities());
		annualEntity.setPriorYearCurrentLiabilities(annualRequest.getPriorYearCurrentLiabilities());
		annualEntity.setNonCurrentAssets(annualRequest.getNonCurrentAssets());
		annualEntity.setFixedAssets(annualRequest.getFixedAssets());
		annualEntity.setCurrentAssets(annualRequest.getCurrentAssets());
		annualEntity.setInventory(annualRequest.getInventory());
		annualEntity.setTradeReceivables(annualRequest.getTradeReceivables());
		annualEntity.setPriorYearNonCurrentAssets(annualRequest.getPriorYearNonCurrentAssets());
		annualEntity.setPriorYearFixedAssets(annualRequest.getPriorYearFixedAssets());
		annualEntity.setPriorYearCurrentAssets(annualRequest.getPriorYearCurrentAssets());
		annualEntity.setPriorYearInventory(annualRequest.getPriorYearInventory());
		annualEntity.setPriorYearTradeReceivables(annualRequest.getPriorYearTradeReceivables());
		annualEntity.setCashFlowFromOperatingActivities(annualRequest.getCashFlowFromOperatingActivities());
		annualEntity.setCapitalExpenditure(annualRequest.getCapitalExpenditure());
		annualEntity.setCashFlowFromInvestingActivities(annualRequest.getCashFlowFromInvestingActivities());
		annualEntity.setCashFlowFromFinancialActivities(annualRequest.getCashFlowFromFinancialActivities());
		annualEntity.setCashAndCashEquivalentAtYearOpening(annualRequest.getCashAndCashEquivalentAtYearOpening());
		annualEntity.setEffectOfForeignExchangeCashAndCashEquivalent(
				annualRequest.getEffectOfForeignExchangeCashAndCashEquivalent());
		if (ValidationUtils.isNotNullAndEmpty(annualRequest.getReportUrl())) {
			annualEntity.setReportURL(annualRequest.getReportUrl());
		}
		logger.info("AnnualDataAssigmentServiceImpl - assignAnnualInput method - end");
	}

	@Override
	public void assignAnalyzedDetails(Map<String, Object> inputParams) throws NoDataException {
		logger.info("AnnualDataAssigmentServiceImpl - assignAnalyzedDetailsToAnnualTable method - start");
		Annual annual = (Annual) inputParams.get(AnnualConstants.ANNUAL_ENTITY);
		boolean isPresent = (boolean) inputParams.get(AnnualConstants.IS_PRESENT);
		String analyzerId = (String) inputParams.get(AnnualConstants.ANALYZER_ID);
		if (isPresent) {
			annual.setLastUpdatedBy(analyzerId);
			annualDAO.updateAnnuallyAnalyzedDetails(annual);
		} else {
			annual.setCreatedBy(analyzerId);
			annual.setLastUpdatedBy(analyzerId);
			annualDAO.insertAnnuallyAnalyzedDetails(annual);
		}
		logger.info("AnnualDataAssigmentServiceImpl - assignAnalyzedDetailsToAnnualTable method - end");
	}

}
