package com.naga.share.market.fundamental.analysis.services.annual;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.annual.AnnualDAO;
import com.naga.share.market.fundamental.analysis.adaptor.company.CompanyDAO;
import com.naga.share.market.fundamental.analysis.exception.InvalidDataException;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.annual.request.AnnualAnalyzeRequestDTO;
import com.naga.share.market.fundamental.analysis.model.annual.response.AnnualResponseDTO;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;
import com.naga.share.market.fundamental.analysis.services.annual.constants.AnnualConstants;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class AnnualServiceImpl implements AnnualService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	AnnualDataRetrivalService annualDataRetrivalService;

	@Autowired
	AnnualDataAssigmentService annualDataAssigmentService;

	@Autowired
	AnnualCalculationService annualCalculationService;

	@Autowired
	AnnualDAO annualDAO;

	@Autowired
	CompanyDAO companyDAO;

	@Override
	public AnnualResponseDTO analyzeAnnually(AnnualAnalyzeRequestDTO annualRequest, String analyzerId)
			throws InvalidDataException, NoDataException {
		logger.info("AnnualServiceImpl - analyzeAnnually method - start");
		Map<String, Object> inputParams = new HashMap<>();
		AnnualResponseDTO annualResponse = new AnnualResponseDTO();
		boolean isPresent = false;
		inputParams.put(AnnualConstants.ANNUAL_REQUEST, annualRequest);
		inputParams.put(AnnualConstants.ANALYZER_ID, analyzerId);
		annualDataAssigmentService.assignSector(inputParams);
		annualDataAssigmentService.assignIndustry(inputParams);
		annualDataAssigmentService.assignCompany(inputParams);
		Annual annualEntity = annualDAO.findByCompanyAndAnalysisYear((Company) inputParams.get(AnnualConstants.COMPANY),
				annualRequest.getAnalysisYear());
		if (ValidationUtils.isNotNull(annualEntity)) {
			isPresent = true;
		} else {
			annualEntity = new Annual();
		}
		inputParams.put(AnnualConstants.IS_PRESENT, isPresent);
		inputParams.put(AnnualConstants.ANNUAL_ENTITY, annualEntity);
		annualDataAssigmentService.assignAnnualInput(inputParams);
		annualCalculationService.annualCalculation(inputParams);
		annualDataAssigmentService.assignAnalyzedDetails(inputParams);
		annualDataRetrivalService.assignRetrivedBasicDetailsResponse(annualResponse, annualEntity);
		annualDataRetrivalService.assignRetrivedAnnualAnalysisInputResponse(annualResponse, annualEntity);
		annualDataRetrivalService.assignRetrivedAnnuallyAnalyzedDetailsResponse(annualResponse, annualEntity);
		logger.info("AnnualServiceImpl - analyzeAnnually method - end");
		return annualResponse;
	}

	@Override
	public void deleteAnalyzedData(String companyName, int analysisYear) throws InvalidDataException, NoDataException {
		logger.info("AnnualServiceImpl - deleteAnalyzedData method - start");
		Company company = companyDAO.findByCompanyName(companyName);
		if (ValidationUtils.isNotNull(company)) {
			annualDAO.deleteByCompanyAndAnalysisYear(company, analysisYear);
		} else {
			logger.error("Invalid company Name : {}", companyName);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR,
					ExceptionsConstants.INVALID_DATA_ERROR);
		}
		logger.info("AnnualServiceImpl - deleteAnalyzedData method - end");
	}

	@Override
	public AnnualResponseDTO retrieveAnalyzedData(String companyName, int analysisYear)
			throws NoDataException, InvalidDataException {
		logger.info("AnnualServiceImpl - retrieveAnalyzedData method - start");
		Company company = companyDAO.findByCompanyName(companyName);
		if (ValidationUtils.isNotNull(company)) {
			Map<String, Object> inputParams = new HashMap<>();
			inputParams.put(AnnualConstants.COMPANY, company);
			inputParams.put(AnnualConstants.ANALYSIS_YEAR, analysisYear);
			AnnualResponseDTO annualResponse = annualDataRetrivalService
					.retriveAnalyzedDetailsForACompanyAndAnalysisYear(inputParams);
			logger.info("AnnualServiceImpl - retrieveAnalyzedData method - end");
			return annualResponse;
		} else {
			logger.error("Invalid company Name : {}", companyName);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR,
					ExceptionsConstants.INVALID_DATA_ERROR);
		}
	}

	@Override
	public AnnualResponseDTO retrieveAnalyzedYears(String companyName) throws NoDataException, InvalidDataException {
		logger.info("AnnualServiceImpl - retrieveAnalyzedYears method - start");
		Company company = companyDAO.findByCompanyName(companyName);
		if (ValidationUtils.isNotNull(company)) {
			AnnualResponseDTO companyAnalyzedYears = new AnnualResponseDTO();
			CompanyResponseDTO companyResponse = new CompanyResponseDTO();
			companyResponse.setCompanyName(company.getCompanyName());
			companyResponse.setCompanySymbol(company.getCompanySymbol());
			List<Annual> analyzedDetails = annualDAO.findAllAnnuallyAnalyzedYearsForACompany(company);
			List<Integer> analysisYears = new ArrayList<Integer>();
			for (Annual annual : analyzedDetails) {
				analysisYears.add(annual.getAnalysisYear());
			}
			companyAnalyzedYears.setCompany(companyResponse);
			companyAnalyzedYears.setAnalysisYears(analysisYears);
			logger.info("AnnualServiceImpl - retrieveAnalyzedData method - end");
			return companyAnalyzedYears;
		} else {
			logger.error("Invalid company Name : {}", companyName);
			throw new InvalidDataException(ExceptionsConstants.INVALID_DATA_ERROR,
					ExceptionsConstants.INVALID_DATA_ERROR);
		}
	}

}
