package com.naga.share.market.fundamental.analysis.services.annual;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.exception.NoDataException;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface AnnualDataAssigmentService {

	/**
	 * Assign sector to a analyzing company
	 * 
	 * @param inputParams
	 * @throws NoDataException
	 */
	public void assignSector(Map<String, Object> inputParams) throws NoDataException;

	/**
	 * Assign sector to a analyzing company
	 * 
	 * @param inputParams
	 * @throws NoDataException
	 */
	public void assignIndustry(Map<String, Object> inputParams) throws NoDataException;

	/**
	 * Assign companyName and symbol to a analyzing company
	 * 
	 * @param inputParams
	 * @throws NoDataException
	 */
	public void assignCompany(Map<String, Object> inputParams) throws NoDataException;

	/**
	 * Method to assign input data to entity for annual analysis
	 * 
	 * @param inputParams
	 */
	public void assignAnnualInput(Map<String, Object> inputParams);

	/**
	 * 
	 * Method to assign Analyzed details to annual table
	 * 
	 * @param inputParams
	 * @throws NoDataException
	 */
	public void assignAnalyzedDetails(Map<String, Object> inputParams) throws NoDataException;

}
