package com.naga.share.market.fundamental.analysis.services.annual;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.exception.InvalidDataException;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.annual.request.AnnualAnalyzeRequestDTO;
import com.naga.share.market.fundamental.analysis.model.annual.response.AnnualResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface AnnualService {

	/**
	 * Method to analyze to a company annually based on annual report
	 * 
	 * @param analyzerId
	 * 
	 * @return AnnualResponse
	 * @throws InvalidDataException
	 * @throws NoDataException
	 */
	public AnnualResponseDTO analyzeAnnually(AnnualAnalyzeRequestDTO annualRequest, String analyzerId)
			throws InvalidDataException, NoDataException;

	/**
	 * Method to delete a annually analyzed details of a company
	 * 
	 * @param annualRequest
	 * @throws InvalidDataException
	 * @throws NoDataException
	 */
	public void deleteAnalyzedData(String companyName, int analysisYear)
			throws InvalidDataException, NoDataException;

	/**
	 * Method to retrieve a annually analyzed details of a company for a year
	 * 
	 * @param companyName
	 * @param analysisYear
	 * @return AnnualResponseDTO
	 * @throws NoDataException
	 * @throws InvalidDataException
	 */
	public AnnualResponseDTO retrieveAnalyzedData(String companyName, int analysisYear)
			throws NoDataException, InvalidDataException;

	/**
	 * 
	 * Method to retrieve a annually analyzed years of a company
	 * 
	 * @param companyName
	 * @return
	 * @throws NoDataException
	 * @throws InvalidDataException
	 */
	public AnnualResponseDTO retrieveAnalyzedYears(String companyName) throws NoDataException, InvalidDataException;
}
