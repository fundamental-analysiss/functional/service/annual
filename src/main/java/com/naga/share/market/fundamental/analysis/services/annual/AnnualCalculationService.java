package com.naga.share.market.fundamental.analysis.services.annual;

import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface AnnualCalculationService {

	/**
	 * 
	 * Service method for annual calculations
	 * 
	 * @param inputParams
	 */
	public void annualCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method for annual basic details calculations
	 * 
	 * @param inputParams
	 */
	public void basicDetailsCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method for annual profitability ratios calculations
	 * 
	 * @param inputParams
	 */
	public void profitabilityRatiosCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method for annual leverage ratios calculations
	 * 
	 * @param inputParams
	 */
	public void leverageRatiosCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method for annual operating ratios calculations
	 * 
	 * @param inputParams
	 */
	public void operatingRatiosCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method for annual valuation ratios calculations
	 * 
	 * @param inputParams
	 */
	public void valuationRatiosCalculation(Map<String, Object> inputParams);

	/**
	 * 
	 * Service method for crucial Details Check calculations
	 * 
	 * @param inputParams
	 */
	public void crucialDetailsCheckCalculation(Map<String, Object> inputParams);
}
